import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shop_app/providers/auth.dart';

import './screens/products_overview_screen.dart';
import './screens/product_detail_screen.dart';
import './screens/shopping_cart_screen.dart';
import './screens/orders_screen.dart';
import './screens/user_products_screen.dart';
import './screens/product_form.dart';
import './screens/auth_screen.dart';
import './screens/splash_screen.dart';

import './providers/products.dart';
import '../providers/shopping_cart.dart';
import '../providers/orders.dart';

import './helpers/custom_route.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, Products>(
          create: (context) => Products(
            Provider.of<Auth>(context, listen: false).token ?? '',
            Provider.of<Auth>(context, listen: false).userId ?? '',
            [],
          ),
          update: (context, auth, previousProducts) => Products(
              auth.token ?? '',
              auth.userId ?? '',
              previousProducts?.items ?? []),
        ),
        ChangeNotifierProvider(
          create: (context) => ShoppingCart(),
        ),
        ChangeNotifierProxyProvider<Auth, Orders>(
          create: (context) => Orders(
            Provider.of<Auth>(context, listen: false).token ?? '',
            Provider.of<Auth>(context, listen: false).userId ?? '',
            [],
          ),
          update: (context, auth, previousOrders) => Orders(
              auth.token ?? '', auth.userId ?? '', previousOrders!.orders),
        ),
      ],
      child: Consumer<Auth>(
        builder: (context, auth, _) {
          return MaterialApp(
            title: 'Shop App',
            theme: ThemeData(
              primarySwatch: Colors.purple,
              colorScheme: ColorScheme.fromSwatch(
                primarySwatch: Colors.purple,
                accentColor: Colors.deepOrange,
              ),
              fontFamily: 'Lato',
              pageTransitionsTheme: PageTransitionsTheme(builders:{
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS:CustomPageTransitionBuilder(),
              } ),
            ),
            home: auth.isAuth
                ? const ProductOverviewScreen()
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (ctx, authResultSnapShot) =>
                        authResultSnapShot.connectionState ==
                                ConnectionState.waiting
                            ? const SplashScreen()
                            : const AuthScreen(),
                  ),
            routes: {
              ProductDetailScreen.routeName: (context) => const ProductDetailScreen(),
              ShoppingCartScreen.routeName: (context) =>
                  const ShoppingCartScreen(),
              OrdersScreen.routeName: (context) => const OrdersScreen(),
              UserProductsScreen.routeName: (context) =>
                  const UserProductsScreen(),
              ProductForm.routeName: (context) => const ProductForm(),
            },
            onUnknownRoute: (setting) {
              return MaterialPageRoute(
                builder: (context) => auth.isAuth
                    ? const ProductOverviewScreen()
                    : const AuthScreen(),
              );
            },
            debugShowCheckedModeBanner: false,
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate
            ],
            supportedLocales: const [
              Locale('nl', 'NL'),
            ],
          );
        },
      ),
    );
  }
}
