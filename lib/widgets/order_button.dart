import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/shopping_cart.dart';
import '../providers/orders.dart';

class OrderButton extends StatefulWidget {
  const OrderButton({
    Key? key,
    required ShoppingCart shoppingCart,
  })  : _shoppingCart = shoppingCart,
        super(key: key);

  final ShoppingCart _shoppingCart;

  @override
  State<OrderButton> createState() => _OrderButtonState();
}

class _OrderButtonState extends State<OrderButton> {
  var _isSending = false;

  @override
  Widget build(BuildContext context) {
    final _scaffoldMessenger = ScaffoldMessenger.of(context);

    return _isSending
        ? const Padding(
          padding: EdgeInsets.symmetric(horizontal:12),
          child: CircularProgressIndicator(),
        )
        : TextButton(
            onPressed: widget._shoppingCart.itemCount == 0||_isSending
                ? null
                : () {
                    setState(() => _isSending = true);
                    Provider.of<Orders>(context, listen: false)
                        .addOrder(
                      widget._shoppingCart.items.values.toList(),
                      widget._shoppingCart.totalAmount,
                    )
                        .then((_) {
                      setState(() => _isSending = false);
                      widget._shoppingCart.clearItems();
                    }).catchError((error) {
                      setState(() => _isSending = false);
                      _scaffoldMessenger.removeCurrentSnackBar();
                      _scaffoldMessenger.showSnackBar(
                        SnackBar(
                          content: Text(
                            error.toString(),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    });
                  },
            child: const Text('ORDER NOW'),
            style: TextButton.styleFrom(
                primary: Theme.of(context).colorScheme.primary),
          );
  }
}
