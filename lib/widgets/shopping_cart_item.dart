import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/shopping_cart.dart';

class ShoppingCartItem extends StatelessWidget {
  final String id;
  final String title;
  final int quantity;
  final double price;
  final String productId;

  const ShoppingCartItem(
      {Key? key,
      required this.id,
      required this.title,
      required this.quantity,
      required this.price,
      required this.productId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(15),
      child: Dismissible(
        key: ValueKey(id),
        direction: DismissDirection.endToStart,
        onDismissed: (_) => Provider.of<ShoppingCart>(context, listen: false)
            .removeItem(productId),
        confirmDismiss: (direction) {
          return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              title: const Text('Are you sure?'),
              content: const Text('Do you want to remove the item from the cart?'),
              actions: [
                TextButton(child: const Text('NO'), onPressed: (){
                  Navigator.pop(context, false);
                },),
                TextButton(child: const Text('YES'), onPressed: (){
                  Navigator.pop(context, true);
                },),
              ],
            ),
          );
        },
        background: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            alignment: Alignment.centerRight,
            color: Theme.of(context).errorColor,
            child: const Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          child: ListTile(
            leading: CircleAvatar(
              child: Padding(
                padding: const EdgeInsets.all(2),
                child: FittedBox(
                  child: Text('€$price'),
                ),
              ),
            ),
            title: Text(title),
            subtitle: Text('total: €${(quantity * price)}'),
            trailing: Text('$quantity x'),
          ),
        ),
      ),
    );
  }
}
