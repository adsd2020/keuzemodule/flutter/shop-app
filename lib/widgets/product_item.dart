import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/product_detail_screen.dart';

import '../providers/product.dart';
import '../providers/shopping_cart.dart';
import '../providers/auth.dart';

class ProductItem extends StatelessWidget {
  const ProductItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _product = Provider.of<Product>(context, listen: false);
    final _shoppingCart = Provider.of<ShoppingCart>(context, listen: false);
    final _authData = Provider.of<Auth>(context, listen: false);
    final _scaffoldMessenger = ScaffoldMessenger.of(context);

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: _product.id,
            );
          },
          child: Hero(
            tag: _product.id,
            child: FadeInImage(
              placeholder:
                  const AssetImage('assets/images/product-placeholder.png'),
              image: NetworkImage(_product.imageUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          leading: Consumer<Product>(
            builder: (ctx, product, child) => IconButton(
              icon: Icon(
                  product.isFavorit ? Icons.favorite : Icons.favorite_border),
              onPressed: () {
                product
                    .toggleFavoritStatus(
                        _authData.token as String, _authData.userId as String)
                    .catchError((error) {
                  _scaffoldMessenger.removeCurrentSnackBar();
                  _scaffoldMessenger.showSnackBar(
                    SnackBar(
                      content: Text(
                        error.toString(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                });
              },
              color: Theme.of(context).colorScheme.secondary,
              // label: child, if you have somthig as label and you dont want to rebuild it
            ),
            // child: const Text('You can use this in the builder and this wil nor rebuild if data changes!'),
          ),
          trailing: IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {
              _shoppingCart.addItem(
                _product.id,
                _product.title,
                _product.price,
              );

              // depricated
              // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Added item to cart!')));
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text('Added item to cart!'),
                  duration: const Duration(seconds: 3),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      _shoppingCart.removeSingleItem(_product.id);
                    },
                  ),
                ),
              );
            },
            color: Theme.of(context).colorScheme.secondary,
          ),
          backgroundColor: Colors.black87,
          title: FittedBox(
            child: Text(
              _product.title,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
