import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/product_item.dart';

import '../providers/products.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavs;
  const ProductsGrid({Key? key, required this.showFavs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _productsData = Provider.of<Products>(context, listen: false);
    final _loadedProduct =
        showFavs ? _productsData.favoriteItems : _productsData.items;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: _loadedProduct.length,
      itemBuilder: (context, i) => ChangeNotifierProvider.value(
        // create: (context) => loadedProduct[i],
        value: _loadedProduct[i],
        child: const ProductItem(),
      ),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}
