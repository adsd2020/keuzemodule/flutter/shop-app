import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/shopping_cart_item.dart';
import '../widgets/order_button.dart';

import '../providers/shopping_cart.dart' show ShoppingCart;

class ShoppingCartScreen extends StatelessWidget {
  static const routeName = "/shopping-cart";

  const ShoppingCartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _shoppingCart = Provider.of<ShoppingCart>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Shopping Cart'),
      ),
      body: Column(
        children: [
          Card(
            margin: const EdgeInsets.all(15),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Total',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const Spacer(),
                  Chip(
                    label: Text(
                      '€ ${_shoppingCart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color:
                            Theme.of(context).primaryTextTheme.headline6!.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).colorScheme.primary,
                  ),
                  OrderButton(shoppingCart: _shoppingCart),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _shoppingCart.itemCount,
              itemBuilder: (ctx, i) => ShoppingCartItem(
                id: _shoppingCart.items.values.toList()[i].id,
                title: _shoppingCart.items.values.toList()[i].title,
                quantity: _shoppingCart.items.values.toList()[i].quantity,
                price: _shoppingCart.items.values.toList()[i].price,
                productId: _shoppingCart.items.keys.toList()[i],
              ),
            ),
          )
        ],
      ),
    );
  }
}
