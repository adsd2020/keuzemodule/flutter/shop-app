import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/shopping_cart_screen.dart';

import '../widgets/products_grid.dart';
import '../widgets/badge.dart';
import '../widgets/app_drawer.dart';

import '../providers/shopping_cart.dart';
import '../providers/products.dart';

enum FilterOption {
  favoriets,
  all,
}

class ProductOverviewScreen extends StatefulWidget {
  const ProductOverviewScreen({Key? key}) : super(key: key);

  @override
  State<ProductOverviewScreen> createState() => _ProductOverviewScreenState();
}

class _ProductOverviewScreenState extends State<ProductOverviewScreen> {
  var _showOnlyFavorites = false;
  var _isLoading = false;

  @override
  void initState() {
    setState(() {
      _isLoading = true;
    });
    Provider.of<Products>(context, listen: false)
        .fetchAndSetProducts()
        .then((_) {
      setState(() {
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var productsContainer = Provider.of<Products>(context, listen:false);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
        actions: [
          PopupMenuButton(
            onSelected: (FilterOption selected) {
              if (selected == FilterOption.favoriets) {
                // productsContainer.showFavoritesOnly();
                setState(() => _showOnlyFavorites = true);
              } else {
                setState(() => _showOnlyFavorites = false);
                // productsContainer.showAll();
              }
            },
            icon: const Icon(Icons.more_vert),
            itemBuilder: (_) => [
              const PopupMenuItem(
                child: Text('Only favoriets'),
                value: FilterOption.favoriets,
              ),
              const PopupMenuItem(
                child: Text('All products'),
                value: FilterOption.all,
              ),
            ],
          ),
          Consumer<ShoppingCart>(
            builder: (_, shoppingCart, btn) => Badge(
              child: btn as Widget,
              value: shoppingCart.itemCount.toString(),
            ),
            child: IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed(ShoppingCartScreen.routeName);
              },
              icon: const Icon(Icons.shopping_cart),
            ),
          )
        ],
      ),
      drawer: const AppDrawer(),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ProductsGrid(
              showFavs: _showOnlyFavorites,
            ),
    );
  }
}
