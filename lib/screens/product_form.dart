import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/product.dart';
import '../providers/products.dart';

class ProductForm extends StatefulWidget {
  static const routeName = '/product-form';

  const ProductForm({Key? key}) : super(key: key);

  @override
  State<ProductForm> createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  var _isInitialized = false;
  var _isLoading = false;

  final _imageUrlController = TextEditingController();
  final _imageFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _product = Product(
    id: '',
    title: '',
    description: '',
    price: 0,
    imageUrl: '',
  );

  //dit works in the new version, but nice to know if i want to focus a specific field
  //if you use focusNode you have to dispose then in the dispose method of widget to clear them from memory
  // var _priceFocusNode = FocusNode();

  @override
  void initState() {
    _imageFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInitialized) {
      final id = ModalRoute.of(context)?.settings.arguments as String?;

      if (id != null) {
        _product = Provider.of<Products>(context, listen: false).findById(id);
        _imageUrlController.text = _product.imageUrl;
      }

      _isInitialized = true;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _imageFocusNode.removeListener(_updateImageUrl);
    _imageUrlController.dispose();
    _imageFocusNode.dispose();
    // _priceFocusNode.dispose();
    super.dispose();
  }

  void _updateImageUrl() {
    if (!_imageFocusNode.hasFocus) {
      setState(() {});
    }
  }

  Future<void> _submitForm() async {
    final isValid = _form.currentState!.validate();
    if (!isValid) return;

    _form.currentState!.save();
    setState(() => _isLoading = true);

    try {
      if (_product.id.isNotEmpty) {
        await Provider.of<Products>(context, listen: false)
            .updateProduct(_product);
      } else {
        await Provider.of<Products>(context, listen: false)
            .addProduct(_product);
      }
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text('An error occured!'),
          // content: Text(error.toString()),
          content: const Text('Something went wrong!'),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: const Icon(Icons.close))
          ],
        ),
      );
    } finally {
      // setState(() => _isLoading = false);
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_product.id == '' ? 'New Product' : 'Edit Product'),
        actions: [
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: _submitForm,
          )
        ],
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16),
              child: Form(
                key: _form,
                child: ListView(
                  children: [
                    TextFormField(
                      initialValue: _product.title,
                      textInputAction: TextInputAction.next,
                      // onFieldSubmitted: (_)=>FocusScope.of(context).requestFocus(_priceFocusNode),
                      decoration: const InputDecoration(
                        labelText: 'Title',
                      ),
                      onSaved: (value) => _product = Product(
                        id: _product.id,
                        title: value as String,
                        description: _product.description,
                        price: _product.price,
                        imageUrl: _product.imageUrl,
                        isFavorit: _product.isFavorit,
                      ),
                      validator: (value) {
                        if (value == null || value.trim().isEmpty) {
                          return 'Please provide a title.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      initialValue: _product.description,
                      // textInputAction: TextInputAction.next,
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      decoration: const InputDecoration(
                        labelText: 'Description',
                      ),
                      onSaved: (value) => _product = Product(
                        id: _product.id,
                        title: _product.title,
                        description: value as String,
                        price: _product.price,
                        imageUrl: _product.imageUrl,
                        isFavorit: _product.isFavorit,
                      ),
                      validator: (value) {
                        if (value == null || value.trim().isEmpty) {
                          return 'Please provide a description.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      initialValue: _product.price.toStringAsFixed(2),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      // focusNode: _priceFocusNode,
                      decoration: const InputDecoration(
                        labelText: 'Price',
                      ),
                      onSaved: (value) => _product = Product(
                        id: _product.id,
                        title: _product.title,
                        description: _product.description,
                        price: double.tryParse(value as String) ?? 0,
                        imageUrl: _product.imageUrl,
                        isFavorit: _product.isFavorit,
                      ),
                      validator: (value) {
                        if (value == null || value.trim().isEmpty) {
                          return 'Please enter a price!';
                        }
                        var price = double.tryParse(value);
                        if (price == null) {
                          return 'Please enter a valid number!';
                        }
                        if (price <= 0) {
                          return 'please enter a number greater than zero!';
                        }
                        return null;
                      },
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: 100,
                          height: 100,
                          margin: const EdgeInsets.only(top: 8, right: 10),
                          child: _imageUrlController.text.isEmpty
                              ? const FittedBox(
                                  child: Text('Enter an image url!'))
                              : FittedBox(
                                  child: Image.network(
                                    _imageUrlController.text,
                                    fit: BoxFit.contain,
                                  ),
                                ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                              decoration:
                                  const InputDecoration(labelText: 'Image Url'),
                              keyboardType: TextInputType.url,
                              textInputAction: TextInputAction.done,
                              controller: _imageUrlController,
                              onFieldSubmitted: (_) => _submitForm(),
                              focusNode: _imageFocusNode,
                              onSaved: (value) => _product = Product(
                                    id: _product.id,
                                    title: _product.title,
                                    description: _product.description,
                                    price: _product.price,
                                    imageUrl: value as String,
                                    isFavorit: _product.isFavorit,
                                  ),
                              validator: (value) {
                                if (value == null || value.trim().isEmpty) {
                                  return 'Please enter an image url!';
                                }
                                return null;
                              }),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
