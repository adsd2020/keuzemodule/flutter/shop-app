import 'dart:convert';
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/http_exeption.dart';

class Auth with ChangeNotifier {
  String? _token;
  DateTime? _expiryTime;
  String? _userId;
  Timer? _authTimer;

  bool get isAuth {
    return token != null;
  }

  String? get userId {
    return _userId;
  }

  String? get token {
    if (_expiryTime != null &&
        _expiryTime!.isAfter(DateTime.now()) &&
        _token != null) {
      // print("time: $_expiryTime en now is: ${DateTime.now()}");
      return _token;
    }
    return null;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url = Uri.parse(
        'https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=AIzaSyALfZMlxmPG6RtOagvEGIH8scg1UbPvSyI');

    try {
      final response = await http.post(
        url,
        body: jsonEncode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );

      final responseData = jsonDecode(response.body);
      if (responseData.containsKey('error')) {
        throw HttpException(responseData['error']['message']);
      }

      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expiryTime = DateTime.now().add(
        Duration(
          seconds: int.parse(responseData['expiresIn']),
        ),
      );
      
      _setAutoLogout();
      notifyListeners();

      final prefs = await SharedPreferences.getInstance();
      final userData = jsonEncode({
        '_token' : _token,
        '_expiryTime' : _expiryTime!.toIso8601String(),
        '_userId' : _userId,
      });
      prefs.setString('ShopAppUserData', userData);


    } catch (error) {
      rethrow;
    }
  }

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password, 'signUp');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'signInWithPassword');
  }

  Future<bool> tryAutoLogin() async{
    final prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey('ShopAppUserData')){
      return false;
    }

    final extractedUserData = jsonDecode(prefs.getString('ShopAppUserData') as String) as Map;
    final expiryTime = DateTime.parse(extractedUserData['_expiryTime']);

    if (expiryTime.isBefore(DateTime.now())) {
      return false;
    }

    _token = extractedUserData['_token'];
    _expiryTime = expiryTime;
    _userId = extractedUserData['_userId'];
    notifyListeners();
    _setAutoLogout();
    return true;
  }

  Future<void> logOut() async{
    _token = null;
    _expiryTime = null;
    _userId = null;
    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }
    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('ShopAppUserData');
    prefs.clear();
  }

  void _setAutoLogout(){
    if (_authTimer != null) {
      _authTimer!.cancel();
    }

    final timeToExpiry = _expiryTime?.difference(DateTime.now()).inSeconds??0;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logOut);
  }
}
