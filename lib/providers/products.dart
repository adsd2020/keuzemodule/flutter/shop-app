import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './product.dart';

import '../models/http_exeption.dart';

class Products with ChangeNotifier {
  final String authToken;
  final String userId;

  List<Product> _items = [];

  Products(this.authToken, this.userId, this._items);

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return [..._items.where((item) => item.isFavorit).toList()];
  }

  Product findById(String id) {
    return _items.firstWhere((item) => item.id == id);
  }

  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    try {
      final filterQuery = filterByUser?'&orderBy="ownerId"&equalTo="$userId"':'';
      final url = Uri.parse(
          'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/products.json?auth=$authToken$filterQuery');

      final response = await http.get(url);
      final extractedDate = jsonDecode(response.body);
      final List<Product> loadedProducts = [];

      if (extractedDate == null) return;

      final favUrl = Uri.parse(
          'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/userFavoriets/$userId.json?auth=$authToken');
      final favoriteResponse = await http.get(favUrl);
      final favoriteData = jsonDecode(favoriteResponse.body);

      (extractedDate as Map<String, dynamic>).forEach((prodId, prodData) {
        loadedProducts.add(
          Product(
            id: prodId,
            title: prodData['title'],
            description: prodData['description'],
            price: prodData['price'],
            imageUrl: prodData['imageUrl'],
            isFavorit:
                favoriteData != null ? favoriteData[prodId] ?? false : false,
          ),
        );
      });

      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> addProduct(Product item) async {
    try {
      final url = Uri.parse(
          'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/products.json?auth=$authToken');

      final response = await http.post(
        url,
        body: json.encode({
          'title': item.title,
          'description': item.description,
          'price': item.price,
          'imageUrl': item.imageUrl,
          'ownerId': userId,
        }),
      );

      // print(jsonDecode(response.body)['name']);
      final newProduct = Product(
        id: jsonDecode(response.body)['name'],
        title: item.title,
        description: item.description,
        price: item.price,
        imageUrl: item.imageUrl,
      );
      _items.add(newProduct);
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> updateProduct(Product item) async {
    final productIndex = _items.indexWhere((prod) => prod.id == item.id);

    if (productIndex >= 0) {
      final url = Uri.parse(
          'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/products/${item.id}.json?auth=$authToken');
      try {
        await http.patch(url,
            body: jsonEncode({
              'title': item.title,
              'description': item.description,
              'price': item.price,
              'imageUrl': item.imageUrl,
            }));
        _items[productIndex] = item;
        notifyListeners();
      } catch (error) {
        rethrow;
      }
    }
  }

  Future<void> deleteProduct(String id) async {
    final productIndex = _items.indexWhere((prod) => prod.id == id);
    final url = Uri.parse(
        'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/products/$id.json?auth=$authToken');

    if (productIndex >= 0) {
      final reserveCopyProduct = _items[productIndex];
      _items.removeWhere((product) => product.id == id);
      notifyListeners();

      final response = await http.delete(url);

      if (response.statusCode >= 400) {
        _items.insert(productIndex, reserveCopyProduct);
        notifyListeners();
        throw HttpException('Could not delete product.');
      }
    }
  }
}
