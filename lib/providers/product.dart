import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/http_exeption.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorit;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorit = false,
  });

  Future<void> toggleFavoritStatus(String token, String userId) async {
    final url = Uri.parse(
        'https://flutter-course-c3a6e-default-rtdb.europe-west1.firebasedatabase.app/userFavoriets/$userId/$id.json?auth=$token');
    isFavorit = !isFavorit;
    notifyListeners();

    final response = await http.put(
      url,
      body: jsonEncode(isFavorit),
    );
    if (response.statusCode >= 400) {
      isFavorit = !isFavorit;
      notifyListeners();
      throw HttpException('Somthing went wrong!');
    }
  }
}
